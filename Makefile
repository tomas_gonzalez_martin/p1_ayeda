BIN = bin
INCLUDE = include
OBJ = obj
SRC = src
TEST = test

EXEC = $(BIN)/p1
LIST_OF_EXECS = $(EXEC) $(EXE)_dll_test $(EXE)_queue_test $(EXE)_sll_test $(EXEC)_stack_test $(EXEC)_vector_test
LIST_OF_CLASSES = $(INCLUDE)/list/dll_node_t.hpp $(INCLUDE)/list/dll_t.hpp $(INCLUDE)/list/sll_node_t.hpp $(INCLUDE)/list/sll_t.hpp $(INCLUDE)/abstractList_t.hpp $(INCLUDE)/queue_t.hpp $(INCLUDE)/stack_t.hpp $(INCLUDE)/vector_t.hpp


# deprecated: No se posee un main principal
# all: stack_build queue_build
# 	g++ -c -g $(SRC)/main.cpp -o $(OBJ)/main.o
# 	g++ -g $(LIST_OF_CLASSES) $(OBJ)/main.o -o $(EXEC)

all: vector sll dll stack queue


clean:
	@echo 'cleaning...'
	rm $(OBJ)/*.o $(LIST_OF_EXECS)

exec: 
	@echo 'executing program'
	./$(EXEC)

# Vector testing
vector_build:
	g++ -c -g $(TEST)/vector_test.cpp -o $(OBJ)/vector_test.o
	g++ -g -o $(EXEC)_vector_test $(INCLUDE)/vector_t.hpp $(OBJ)/vector_test.o

vector: vector_build
	@echo 'vector_t testing:'
	./$(EXEC)_vector_test


# sll testing
sll_build:
	g++ -c -g $(TEST)/list/sll_test.cpp -o $(OBJ)/list/sll_test.o
	g++ -g $(INCLUDE)/list/sll_t.hpp $(INCLUDE)/list/sll_node_t.hpp $(OBJ)/list/sll_test.o -o $(EXEC)_sll_test 

sll: sll_build
	@echo 'sll_t testing:'
	./$(EXEC)_sll_test



dll_build:
	g++ -c -g $(TEST)/list/dll_test.cpp -o $(OBJ)/list/dll_test.o
	g++ -g $(INCLUDE)/list/dll_t.hpp $(INCLUDE)/list/dll_node_t.hpp $(OBJ)/list/dll_test.o -o $(EXEC)_dll_test 

dll: dll_build
	@echo 'dll_t testing:'
	./$(EXEC)_dll_test

# stack testing
stack_build: 
	g++ -c -g $(TEST)/stack_test.cpp -o $(OBJ)/stack_test.o
	g++ -g $(INCLUDE)/stack_t.hpp $(INCLUDE)/list/sll_node_t.hpp $(OBJ)/stack_test.o -o $(EXEC)_stack_test


stack: stack_build
	@echo 'stack_t testing:'
	./$(EXEC)_stack_test


# queue testing
queue_build:
	g++ -c -g $(TEST)/queue_test.cpp -o $(OBJ)/queue_test.o
	g++ -g $(INCLUDE)/queue_t.hpp $(INCLUDE)/list/sll_node_t.hpp $(OBJ)/queue_test.o -o $(EXEC)_queue_test


queue: queue_build
	@echo 'queue_t testing:'
	./$(EXEC)_queue_test


mod_build : 
	g++ -g $(SRC)/asignatura_libro.cpp -o $(OBJ)/mod.o
	g++ -g $(INCLUDE)/list/dll_t.hpp $(INCLUDE)/vector_t.hpp -o $(EXEC)_mod

mod: mod_build
	./$(EXEC)_mod

help:
	@echo 'make <option>'
	@echo 'options:'
	@echo '		clean -> clear *.o and executables'
	@echo '		vector -> build and run vector test'
	@echo '		sll -> build and run sll test'
	@echo '		dll -> build and run dll test'
	@echo '		stack -> build and run stack test'
	@echo '		queue -> build and run queue test'
