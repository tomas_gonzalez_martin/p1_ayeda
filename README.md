# Práctica 1: Implementación de Estructuras de Datos
### Autor
Tomás González Martín  
alu0100913033@ull.edu.es

### Breve de la práctica
Se pretende implementar las siguientes estructuras de datos con el uso de plantillas:
* Vector
* Listas enlazadas
* Pila
* Cola

## Estructura del código
```
├── bin
│   ├── p1
│   ├── p1_dll_test
│   ├── p1_queue_test
│   ├── p1_sll_test
│   ├── p1_stack_test
│   └── p1_vector_test
├── doc
│   └── notas_de_la_clase_tutorizada.odt
├── include
│   ├── abstractList_t.hpp
│   ├── list
│   │   ├── dll_node_t.hpp
│   │   ├── dll_t.hpp
│   │   ├── sll_node_t.hpp
│   │   └── sll_t.hpp
│   ├── queue_t.hpp
│   ├── stack_t.hpp
│   └── vector_t.hpp
├── lib
│   └── common_libs.hpp
├── Makefile
├── obj
│   ├── list
│   │   ├── dll_test.o
│   │   └── sll_test.o
│   ├── main.o
│   ├── queue_test.o
│   ├── stack_test.o
│   └── vector_test.o
├── README.md
├── src
│   └── main.cpp
└── test
    ├── list
    │   ├── dll_test.cpp
    │   └── sll_test.cpp
    ├── queue_test.cpp
    ├── stack_test.cpp
    └── vector_test.cpp

```
* En el directorio test se posee un conjunto de ficheros principales (con su respectivo main) que ejecuta pruebas de su estructura específica.
* El */src/main* está en estado *deprecated* porque se han encapsulado las pruebas de la forma especificada en el punto anterior.
* */lib/common_libs.hpp* posee las cabeceras comunes utilizadas por las estructuras de datos. 
* Las listas están en un directorio de mayor profundidad (*list*).

## Uso del makefile
La ayuda es visible utilizando la etiqueta help:
```
$ make help
make <option>
options:
                clean -> clear *.o and executables
                vector -> build and run vector test
                sll -> build and run sll test
                dll -> build and run dll test
                stack -> build and run stack test
                queue -> build and run queue test
```
Si no se especifica ninguna de las opciones, se ejecutarán todas ellas (en ese orden) excepto *clean*