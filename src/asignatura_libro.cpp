#include "../include/list/dll_t.hpp"
#include "../include/vector_t.hpp"
#include <string>

struct Libro{
    string isbn;
    string titulo;
};


struct Asignatura{
    string codigo;
    string nombre;
    dll_t<Libro> bibliografia;
};




void verBibliografia(vector_t<Asignatura> vectorAsignaturas, string codigo){

    for (int i = 0; i < vectorAsignaturas.getSize(); i++)
        if (vectorAsignaturas[i].codigo == codigo)
            // falla por cómo está definido el write() -> ¿Especificar la template?
            cout << vectorAsignaturas[i].bibliografia << endl;
        
    

}



int main (void){
    Libro libroComputacion;
    libroComputacion.isbn = "aaa";
    libroComputacion.titulo = "Analisis computacional I";

    Libro libroComputacion2;
    libroComputacion.isbn = "aaa2";
    libroComputacion.titulo = "Analisis computacional II";

    Libro libroBiologia;
    libroBiologia.isbn = "bbb";
    libroBiologia.titulo = "Biologia I";

    Libro libroBiologia2;
    libroBiologia.isbn = "bbb2";
    libroBiologia.titulo = "Biologia II";


    Asignatura asignatura1;
    asignatura1.codigo = "abcde";
    asignatura1.nombre = "AyEDA";
    asignatura1.bibliografia.insertHead(new dll_node_t<Libro> (libroComputacion));
    asignatura1.bibliografia.insertHead(new dll_node_t<Libro> (libroBiologia));


    Asignatura asignatura2;
    asignatura2.codigo = "ffsdfasd";
    asignatura2.nombre = "Biologia";
    asignatura2.bibliografia.insertHead(new dll_node_t<Libro> (libroBiologia));
    asignatura2.bibliografia.insertHead(new dll_node_t<Libro> (libroBiologia2));
    asignatura2.codigo = "wxyz";

    vector_t<Asignatura> vectorAsignaturas(2);

    vectorAsignaturas[0] = asignatura1;

    vectorAsignaturas[1] = asignatura2;

    verBibliografia(vectorAsignaturas, "abcde");

    return 0;
}