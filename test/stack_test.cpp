#include "../include/stack_t.hpp"



void stack_test(void){

    stack_t<float> stackFloat(1.2);
    stack_t<char> stackChar;

    // Acceso a los datos
    // - push
    // - top
    // - pop
    stackFloat.push(1.44);
    stackFloat.push(4.21);
    stackFloat.push(3.44);

    assert( abs(stackFloat.top() - 3.44) < EPSILON );

    stackFloat.pop();

    // Información:
    // - empty 
    assert(stackChar.empty());

    cout << stackFloat << endl;
    cout << stackChar << endl;
    
    cout << "\n\tstack_t works successfully" << endl << endl;

}




int main (void){

    stack_test();

    return 0;
}