#include "../include/queue_t.hpp"



void queue_test(void){

    queue_t<float> queueFloat(1.2);
    queue_t<char> queueChar;

    // Acceso a los datos
    // - push
    // - top
    // - pop
    queueFloat.push(1.44);
    queueFloat.push(4.21);
    queueFloat.push(3.44);
    assert( abs(queueFloat.top() - 1.2) < EPSILON );

    queueFloat.pop();

    // Información:
    // - empty 
    assert(queueChar.empty());

    // Se espera 1.44 y 4.21
    cout << queueFloat << endl;
    cout << queueChar << endl;
    
    cout << "\n\tqueue_t works successfully" << endl << endl;

}




int main (void){

    queue_test();

    return 0;
}