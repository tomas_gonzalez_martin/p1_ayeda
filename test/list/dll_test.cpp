

#include "../../include/list/dll_t.hpp"



void dll_test(void){
 
    dll_t<char> dllViewer;

    // Accesos a la dll
    // - insertTail
    // - extractTail


    dllViewer.insertTail(new dll_node_t<char>('T'));
    dllViewer.insertTail(new dll_node_t<char>('T'));
    dllViewer.insertTail(new dll_node_t<char>('T'));
    dllViewer.insertHead(new dll_node_t<char>('H'));
    dllViewer.insertHead(new dll_node_t<char>('H'));
    dllViewer.insertHead(new dll_node_t<char>('H'));

    // Escritura sobre la dll
    // Expectativa: [H, H, T, T]
    cout << dllViewer << endl;

    // Quitamos una cola
    dllViewer.extractTail();

    // Expectativa: [H, H, T]
    cout << dllViewer << endl;

    // Quitamos una head
    dllViewer.extractHead();

    // Expectativa: [H, T]
    cout << dllViewer << endl;


    // Información sobre la dll
    assert(!dllViewer.empty());


    cout << "\n\tdll_test works successfully" << endl;
}

void dll_node_test(void){

    dll_node_t<int> emptyNode;
    dll_node_t<int> valueNode(3);
    // Accesos al nodo
    // - setNext
    // - getNext
    emptyNode.setNext(new dll_node_t<int> (2));
    assert(emptyNode.getNext()->getValue() == 2);

    // - setValue
    // - getValue
    valueNode.setValue(2);
    assert(valueNode.getValue() == 2);

    // Operators ya contemplados

    // Visualización
    cout << valueNode << endl;

    // No se deberia mostrar un nodo vacío
    // cout << emptyNode << endl;
    cout << "\n\tdll_node_test works successfully" << endl;

}



int main (void){
    dll_test();
    dll_node_test();
    return 0;
}