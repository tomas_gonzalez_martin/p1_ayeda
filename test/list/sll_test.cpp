#include "../../include/list/sll_t.hpp"



void sll_test(void){
 
    sll_t<int> sllInt;
    sll_t<char> sllViewer;

    sll_node_t<int> valueNode(3);

    // Accesos a la sll
    // - insertHead
    // - extractHead
    sllViewer.insertHead(new sll_node_t<char>('V'));
    sllInt.insertHead(new sll_node_t<int>(3));
    assert(valueNode == sllInt.extractHead());

    // Información sobre la sll
    assert(sllInt.empty());

    // Escritura sobre la sll
    cout << sllInt << endl;
    cout << sllViewer << endl;

    cout << "\n\tsll_test works successfully" << endl;
}

void sll_node_test(void){

    sll_node_t<int> emptyNode;
    sll_node_t<int> valueNode(3);
    sll_node_t<int> *insertedNode;

    // Accesos al nodo
    // - setNext
    // - getNext
    emptyNode.setNext(insertedNode);
    assert(emptyNode.getNext() == insertedNode);

    // - setValue
    // - getValue
    valueNode.setValue(2);
    assert(valueNode.getValue() == 2);

    // Operators ya contemplados

    // Visualización
    cout << valueNode << endl;
    cout << emptyNode << endl;
    cout << "\n\tsll_node_test works successfully" << endl;

}



int main (void){
    sll_test();
    sll_node_test();
    return 0;
}
