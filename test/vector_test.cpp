#include "../include/vector_t.hpp"
#define VECTOR_SIZE 4

/**
 * Implementación de test unitarios simples
 * con el uso de assert
 */


void vector_test(void){
// 3 tipos de constructor
    vector_t<double> vectorDouble;
    vector_t<unsigned int> vectorUInt(VECTOR_SIZE);
    vector_t<double> vectorCopyDouble(vectorDouble);
    


    // 1º Métodos de modificación 
    // - pushBack
    // - swap
    vectorUInt.pushBack(3);
    vectorUInt.pushBack(9);
    vectorCopyDouble.pushBack(3.333);

    vectorUInt.swap(VECTOR_SIZE, VECTOR_SIZE + 1);

    // Asserts sobre vectorUInt
    assert(
        vectorUInt[VECTOR_SIZE] == 9 &&
        vectorUInt[VECTOR_SIZE+1] == 3
    );

    // Assert sobre el vectorCopyDouble
    assert(
        abs(vectorCopyDouble[0] - 3.333) < EPSILON
    );




    // Métodos de información
    // - empty
    // - getSize()

    assert(
        vectorUInt.empty() == false &&
        vectorDouble.empty() == true &&
        vectorCopyDouble.empty() == false
    );


    assert(
        vectorUInt.getSize() == VECTOR_SIZE+2 &&
        vectorDouble.getSize() == 0 &&
        vectorCopyDouble.getSize() == 1
    );



    // Métodos de acceso
    // - at
    // - front
    // - back
    assert(
        vectorUInt[0] == 
        vectorUInt.back() == 
        vectorUInt.at(0) == 
        0
    );


    assert (
        vectorUInt[vectorUInt.getSize()-1] == 3 &&
        vectorUInt.front() == 3 &&
        vectorUInt.at(vectorUInt.getSize()-1) == 3
    );
    
    vectorDouble.pushBack(2.0);

    assert(
        abs(vectorDouble[0] - 2.0) < EPSILON &&
        abs(vectorDouble.at(0) - 2.0) < EPSILON
    );


    // Operadores de visualización
    // cout << vectorUInt << endl;
    // cout << vectorDouble << endl;


    cout << "\n\tvector_t works successfully" << endl << endl;
}

int main (void){
    vector_test();
    return 0;
}