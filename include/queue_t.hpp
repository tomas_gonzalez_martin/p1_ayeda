#ifndef __QUEUE__
#define __QUEUE__

#include "abstractList_t.hpp"

template<class TDato>
class queue_t: public abstractList_t<TDato>{

public:
    queue_t(void);
    queue_t(const TDato& value);
    ~queue_t(void);

    // Acceso a los datos
    const TDato& top(void);
    void pop(void);
    


};
template<class TDato>
queue_t<TDato>::queue_t(void):
abstractList_t<TDato>(){}


template<class TDato>
queue_t<TDato>::queue_t(const TDato& value):
abstractList_t<TDato>(value){}

template<class TDato>
queue_t<TDato>::~queue_t(void){}

template<class TDato>
const TDato& queue_t<TDato>::top(void){
    return abstractList_t<TDato>::list_.getTail()->getValue();
}

template<class TDato>
void queue_t<TDato>::pop(void){
    abstractList_t<TDato>::list_.extractTail();
}



#endif