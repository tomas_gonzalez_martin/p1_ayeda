#ifndef __SLL__
#define __SLL__

#include "sll_node_t.hpp"

template<class TDato>
class sll_t{

public:
    sll_t(void);
    // sll_t(sll_t<TDato>& otherSll);
    ~sll_t(void);

    // Accesos a la sll
    void insertHead(sll_node_t<TDato>* node);
    sll_node_t<TDato>* extractHead(void);
    sll_node_t<TDato>* getHead(void) const;

    void insertAfter(sll_node_t<TDato>* prev, sll_node_t<TDato>* node);
    sll_node_t<TDato>* extractAfter(sll_node_t<TDato>* prev);
    // Información sobre la sll
    bool empty(void) const;

    
    // Visualización de la sll
    ostream& write(ostream& os) const;


private:
    sll_node_t<TDato> *head_;
};


template<class TDato>
ostream& operator<<(ostream& os, sll_t<TDato>& sll){
    return sll.write(os);
}

template<class TDato>
sll_t<TDato>::sll_t(void):
head_(NULL){}

// template<class TDato>
// sll_t<TDato>::sll_t(sll_t<TDato>& otherSll):
// head_(otherSll->getNext()){}

template<class TDato>
sll_t<TDato>::~sll_t(void){
    while(!empty()){
        sll_node_t<TDato> *aux = head_;
        head_ = head_->getNext();
        delete aux;
    }
}


// Accesos a la sll
template<class TDato>
void sll_t<TDato>::insertHead(sll_node_t<TDato>* node){
    node->setNext(head_);
    head_ = node;
}

template<class TDato>
sll_node_t<TDato>* sll_t<TDato>::extractHead(void){
    sll_node_t<TDato> *aux = head_;
    head_ = head_->getNext();
    aux->setNext(NULL);

    return aux;
}

template<class TDato>
sll_node_t<TDato>* sll_t<TDato>::getHead(void) const{
    return head_;
}



template<class TDato>
void sll_t<TDato>::insertAfter(sll_node_t<TDato>* prev, sll_node_t<TDato>* node){
    node->setNext(prev->getNext());
    prev->setNext(node);
}

template<class TDato>
sll_node_t<TDato>* sll_t<TDato>::extractAfter(sll_node_t<TDato>* prev){
    
    sll_node_t<TDato> *aux = prev->getNext();
    prev->setNext(aux->getNext());
    aux->setNext(NULL);

    return aux;
}

// Información de la sll
template<class TDato>
bool sll_t<TDato>::empty(void) const{
    return head_ == NULL;
}


// Visualización de la sll
template<class TDato>
ostream& sll_t<TDato>::write(ostream& os) const{

    sll_node_t<TDato> *nodeReference = head_;
    os << "[";
    while(nodeReference != NULL){
        nodeReference->write(os);
        nodeReference = nodeReference->getNext();
        nodeReference != NULL ? os << ",": os << "";
    } 

    return os << "]";
}

#endif