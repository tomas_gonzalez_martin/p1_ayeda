#ifndef __DLL_NODE__
#define __DLL_NODE__

#include "../../lib/common_libs.hpp"


template<class TDato>
class dll_node_t{

public:
    dll_node_t();
    dll_node_t(const TDato& value);
    ~dll_node_t(void);
    
    // Accesos al nodo
    void setPrev(dll_node_t<TDato>* prev);
    dll_node_t<TDato>* getPrev(void) const;

    // Accesos al nodo
    void setNext(dll_node_t<TDato>* next);
    dll_node_t<TDato>* getNext(void) const;

    void setValue(const TDato& value);
    const TDato& getValue(void);

    // Información
    bool empty(void) const;

    // Operators
    bool operator==(const dll_node_t<TDato>* otherNode) const;
   
    // Visualización
    ostream& write(ostream&) const;
private: 
    dll_node_t<TDato>   *next_;
    dll_node_t<TDato>   *prev_;
    TDato               value_;
};

template<class TDato>
ostream& operator<<(ostream& os, dll_node_t<TDato>& dll_node){
    return dll_node.write(os);
}

template<class TDato>
dll_node_t<TDato>::dll_node_t():
next_(NULL),
prev_(NULL){}


template<class TDato>
dll_node_t<TDato>::dll_node_t(const TDato& value):
next_(NULL),
prev_(NULL),
value_(value){}

template<class TDato>
dll_node_t<TDato>::~dll_node_t(void){
    next_ = NULL;
    prev_ = NULL;
}



template<class TDato>
void dll_node_t<TDato>::setPrev(dll_node_t<TDato>* prev){
    prev_ = prev;
}


template<class TDato>
dll_node_t<TDato>* dll_node_t<TDato>::getPrev(void) const{
    return prev_;
}




template<class TDato>
void dll_node_t<TDato>::setNext(dll_node_t<TDato>* next){
    next_ = next;
}

template<class TDato>
dll_node_t<TDato>* dll_node_t<TDato>::getNext(void) const{
    return next_;
}


template<class TDato>
void dll_node_t<TDato>::setValue(const TDato& value){
    value_ = value;
}

template<class TDato>
const TDato& dll_node_t<TDato>::getValue(void){
    return value_;
}


template<class TDato>
bool dll_node_t<TDato>::empty(void) const{
    return prev_ == NULL && next_ == NULL;
}


template<class TDato>
bool dll_node_t<TDato>::operator==(const dll_node_t<TDato>* otherNode) const{
    return this == otherNode && this->prev_ == otherNode->prev_;
}


template<class TDato>
ostream& dll_node_t<TDato>::write(ostream& os) const{
    os << value_;
    return os;
}
#endif