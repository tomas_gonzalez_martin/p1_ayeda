#ifndef __DLL__
#define __DLL__

#include "dll_node_t.hpp"

template<class TDato>
class dll_t{

public:
    dll_t(void);
    // dll_t(dll_t<TDato>& otherDll);
    ~dll_t(void);

    // Accesos a la dll
    void insertTail(dll_node_t<TDato>* node);
    dll_node_t<TDato>* extractTail(void);
    dll_node_t<TDato>* getTail(void) const;

    // Accesos a la dll
    void insertHead(dll_node_t<TDato>* node);
    dll_node_t<TDato>* extractHead(void);
    dll_node_t<TDato>* getHead(void) const;

    void insertAfter(dll_node_t<TDato>* prev, dll_node_t<TDato>* node);
    dll_node_t<TDato>* extractAfter(dll_node_t<TDato>* prev);
    // Información sobre la dll
    bool empty(void) const;
    
    bool operator==(const dll_t<TDato>& otherDll) const;
    // Visualización de la dll
    ostream& write(ostream& os) const;

private:
    dll_node_t<TDato> *head_;
    dll_node_t<TDato> *tail_;
};


template<class TDato>
ostream& operator<<(ostream& os, dll_t<TDato>& dll){
    return dll.write(os);
}

template<class TDato>
dll_t<TDato>::dll_t(void):
head_(NULL),
tail_(NULL){

}


// template<class TDato>
// dll_t<TDato>::dll_t(dll_t<TDato>& otherDll):
// head_(otherDll.getHead()),
// tail_(otherDll.getTail()){

// }

template<class TDato>
dll_t<TDato>::~dll_t(void){
    while(head_ != NULL){
        dll_node_t<TDato> *aux = head_;
        head_ = head_->getNext();
        delete aux;
    }
}


template<class TDato>
void dll_t<TDato>::insertTail(dll_node_t<TDato>* node){
    
    if (empty()){
        head_ = node;
        tail_ = node;
    }
    else{
        tail_->setNext(node);
        node->setPrev(tail_);
        tail_ = node;
    }

}

template<class TDato>
dll_node_t<TDato>* dll_t<TDato>::extractTail(void){

    assert(!empty());

    dll_node_t<TDato> *aux = tail_;
    tail_ = tail_->getPrev();
    tail_->setNext(NULL);
    aux->setPrev(NULL);

    return aux;
    
}

template<class TDato>
dll_node_t<TDato>* dll_t<TDato>::getTail(void) const{
    return tail_;
}



// Accesos a la dll
template<class TDato>
void dll_t<TDato>::insertHead(dll_node_t<TDato>* node){

    if (empty()){
        head_ = node;
        tail_ = node;
    }
    else{
        head_->setPrev(node);
        node->setNext(head_);
        head_ = node;
    }
    
    

}

template<class TDato>
dll_node_t<TDato>* dll_t<TDato>::extractHead(void){

    assert(!empty());

    dll_node_t<TDato> *aux = head_;
    head_ = head_->getNext();
    head_->setPrev(NULL);
    aux->setNext(NULL);

    return aux;
}

template<class TDato>
dll_node_t<TDato>* dll_t<TDato>::getHead(void) const{
    return head_;
}



template<class TDato>
void dll_t<TDato>::insertAfter(dll_node_t<TDato>* prev, dll_node_t<TDato>* node){
    node->setNext(prev->getNext());
    prev->setNext(node);
}

template<class TDato>
dll_node_t<TDato>* dll_t<TDato>::extractAfter(dll_node_t<TDato>* prev){
    dll_node_t<TDato> *aux = prev->getNext();
    prev->setNext(aux->getNext());
    aux->setNext(NULL);

    return aux;
}



template<class TDato>
ostream& dll_t<TDato>::write(ostream& os) const{

    dll_node_t<TDato> *nodeReference = head_;
    os << "[";
    while(nodeReference != NULL){
        nodeReference->write(os);
        nodeReference = nodeReference->getNext();
        nodeReference != NULL ? os << ", ": os << "";
    } 
    
    os << "]";
    return os;
}




template<class TDato>
bool dll_t<TDato>::empty(void) const{
    return head_ == NULL && tail_ == NULL;
}



template<class TDato>
bool dll_t<TDato>::operator==(const dll_t<TDato>& otherDll) const{
    return head_ == otherDll.getHead() && tail_ == otherDll.getTail();
}

#endif