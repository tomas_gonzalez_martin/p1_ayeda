#ifndef __SLL_NODE__
#define __SLL_NODE__

#include "../../lib/common_libs.hpp"


template<class TDato>
class sll_node_t{

public:
    sll_node_t();
    sll_node_t(const TDato& value);
    ~sll_node_t(void);

    // Accesos al nodo
    void setNext(sll_node_t<TDato>* next);
    sll_node_t<TDato>* getNext(void) const;

    void setValue(const TDato& value);
    const TDato& getValue(void);

    // Operadores
    bool operator==(const sll_node_t<TDato>* otherNode) const;

    // Visualización
    ostream& write(ostream&) const;

private:
    sll_node_t<TDato>   *next_;
    TDato               value_;
};

template<class TDato>
ostream& operator<<(ostream& os, sll_node_t<TDato>& sll_node){
    return sll_node.write(os);
}



template<class TDato>
sll_node_t<TDato>::sll_node_t():
next_(NULL),
value_(){}

template<class TDato>
sll_node_t<TDato>::sll_node_t(const TDato& value):
next_(NULL),
value_(value){}

template<class TDato>
sll_node_t<TDato>::~sll_node_t(void){
    // value_ = NULL;
    next_ = NULL;
}

template<class TDato>
void sll_node_t<TDato>::setNext(sll_node_t<TDato>* next){
    next_ = next;
}

template<class TDato>
sll_node_t<TDato>* sll_node_t<TDato>::getNext(void) const{
    return next_;
}


template<class TDato>
void sll_node_t<TDato>::setValue(const TDato& value){
    value_ = value;
}

template<class TDato>
const TDato& sll_node_t<TDato>::getValue(void){
    return value_;
}



template<class TDato>
bool sll_node_t<TDato>::operator==(const sll_node_t<TDato>* otherNode) const{
    return value_ == otherNode->value_ && next_ == otherNode->next_;
}

template<class TDato>
ostream& sll_node_t<TDato>::write(ostream& os) const{
    os << value_;
    return os;
}

#endif