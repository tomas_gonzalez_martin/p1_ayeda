#ifndef __STACK__
#define __STACK__

#include "abstractList_t.hpp"

template<class TDato>
class stack_t: public abstractList_t<TDato>{

public:
    stack_t(void);
    stack_t(const TDato& value);
    ~stack_t(void);

    // Acceso a los datos
    const TDato& top(void);
    void pop(void);


};

// template<class TDato>
// ostream& operator<<(ostream& os, const stack_t<TDato>& stack){
//     return stackwrite(os);
// }


template<class TDato>
stack_t<TDato>::stack_t(void):
abstractList_t<TDato>(){}


template<class TDato>
stack_t<TDato>::stack_t(const TDato& value):
abstractList_t<TDato>(value){}

template<class TDato>
stack_t<TDato>::~stack_t(void){}


template<class TDato>
const TDato& stack_t<TDato>::top(void){
    return abstractList_t<TDato>::list_.getHead()->getValue();
}

template<class TDato>
void stack_t<TDato>::pop(void){
    abstractList_t<TDato>::list_.extractHead();
    
}




#endif