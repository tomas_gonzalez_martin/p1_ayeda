// #pragma once
#ifndef __VECTOR__
#define __VECTOR__

#include "../lib/common_libs.hpp"

template<class TDato>
class vector_t{

public:
    vector_t();
    vector_t(int size);
    vector_t(const vector_t<TDato>& otherVector_t);
    ~vector_t();

    // Access
    TDato at(int i) const;
    TDato& at(int i);
    TDato front(void) const;
    TDato back(void) const;

    // Information
    bool empty(void) const;
    int getSize(void) const;

    // Modification
    void pushBack(TDato data);
    void swap(int i, int j);

    // Operadores
    TDato& operator[](int i);
    TDato operator[](int i) const;

    // Auxiliar de operador
    ostream& write(ostream& os) const;


private:
    void clone(const vector_t<TDato>& otherVector_t);
    void clear(void);
    void build(int size);
    void resize(int size);


    TDato   *v_;
    uint_t  size_;
};

template<class TDato>
ostream& operator<<(ostream& os, const vector_t<TDato>& vector);



template<class TDato>
vector_t<TDato>::vector_t(){
    build(0);
}


template<class TDato>
vector_t<TDato>::vector_t(int size){
    build(size);
}


template<class TDato>
vector_t<TDato>::vector_t(const vector_t<TDato>& otherVector_t){
    build(otherVector_t.getSize());
    clone(otherVector_t);
}


template<class TDato>
vector_t<TDato>::~vector_t(void){
    clear();
}


// Access
template<class TDato>
TDato vector_t<TDato>::at(int i) const{
    assert(i < size_ && i >= 0);
    return v_[i];
}


template<class TDato>
TDato& vector_t<TDato>::at(int i){
    assert(i < size_ && i >= 0);
    return v_[i];
}

template<class TDato>
TDato vector_t<TDato>::front() const{
    return v_[size_-1];
}


template<class TDato>
TDato vector_t<TDato>::back() const{
    return v_[0];
}



// Information
template<class TDato>
bool vector_t<TDato>::empty(void) const{
    return !size_;
}

template<class TDato>
int vector_t<TDato>::getSize(void) const{
    return (int)size_;
}



// Modification
template<class TDato>
void vector_t<TDato>::pushBack(TDato data){

   vector_t<TDato> cloneVector(*this);
   resize(size_ + 1);
   clone(cloneVector);
   v_[size_-1] = data;
}


template<class TDato>
void vector_t<TDato>::swap(int i, int j){
    TDato aux = v_[i];
    v_[i] = v_[j];
    v_[j] = aux;
}

template<class TDato>
TDato& vector_t<TDato>::operator[](int i){
    return at(i);
}

template<class TDato>
TDato vector_t<TDato>::operator[](int i) const{
    return at(i);
}

template<class TDato>
ostream& vector_t<TDato>::write(ostream& os) const{
    
    os << "[";

    for (int i = 0; i < size_-1; i++)
        os << v_[i] << ", ";
    
    os << v_[size_-1] << "]" << endl;
    return os;
}

template<class TDato>
ostream& operator<<(ostream& os, const vector_t<TDato>& vector){
    return vector.write(os);
}

/**
 * Métodos privados
*/
template<class TDato>
void vector_t<TDato>::clone(const vector_t<TDato>& otherVector_t){
    for (int i = 0; i < size_ && i < otherVector_t.getSize(); i++)
        v_[i] = otherVector_t[i];
}


template<class TDato>
void vector_t<TDato>::clear(void){
    if (v_ != NULL){
        delete [] v_;
        size_ = 0;
    }
}

template <class TDato>
void vector_t<TDato>::build(int size)
{
    assert(size >= 0);
    size_ = (uint_t)size;
    size_ ? v_ = new TDato [size_]: v_ = NULL;
    
}


template<class TDato>
void vector_t<TDato>::resize(int size){
    clear();
    build(size);
}


#endif