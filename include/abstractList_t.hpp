#ifndef __ABSTRACT_LIST__
#define __ABSTRACT_LIST__


#include "../include/list/dll_t.hpp"

template<class TDato>
class abstractList_t{

public:
    abstractList_t(void);
    abstractList_t(const TDato& value);
    virtual ~abstractList_t(void);

    // Acceso a los datos
    // Acceso a los datos
    void push(const TDato& value);
    virtual const TDato& top(void) = 0;
    virtual void pop(void) = 0;

    // Información del stack
    bool empty(void) const;

    ostream& write(ostream& os) const; 

protected:
    dll_t<TDato> list_;

};

template<class TDato>
ostream& operator<<(ostream& os, const abstractList_t<TDato>& list){
    return list.write(os);
}


template<class TDato>
abstractList_t<TDato>::abstractList_t(void):
list_(){}


template<class TDato>
abstractList_t<TDato>::abstractList_t(const TDato& value):
list_(){
    push(value);
}



template<class TDato>
abstractList_t<TDato>::~abstractList_t(void){}

#endif




template<class TDato>
void abstractList_t<TDato>::push(const TDato& value){
    list_.insertHead(new dll_node_t<TDato>(value));
}



template<class TDato>
bool abstractList_t<TDato>::empty(void) const{
    return list_.empty();
}

template<class TDato>
ostream& abstractList_t<TDato>::write(ostream& os) const{
    return list_.write(os);
}